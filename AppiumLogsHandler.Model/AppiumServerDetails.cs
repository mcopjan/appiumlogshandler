﻿using System;

namespace AppiumLogsHandler.Model
{
    public class AppiumServerDetails
    {

        public string IpAddress { get; set; }
        public DateTime StartTime { get; set; }
        public string ServerVersion { get; set; }
    }
}