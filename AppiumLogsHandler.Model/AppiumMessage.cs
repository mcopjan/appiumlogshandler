﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AppiumLogsHandler.Model
{
    public class AppiumLogMessage : ICloneable
    {
        [BsonId]
        private ObjectId Id { get; set; }
        public string Message { get; set; }
        public string Level { get; set; }
        public DateTime CreationTime { get; set; }

        public object Clone()
        {
            return (AppiumLogMessage)this.MemberwiseClone();
        }
    }
}