﻿namespace AppiumLogsHandler.UI.Data
{
    public class MyOptions
    {
        public MyOptions()
        {
            // Set default value.
            ApiUrl = "http://localhost:5000/";
        }

        public string ApiUrl { get; set; }
    }
}
