using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using AppiumLogsHandler.Model;
using Microsoft.Extensions.Options;
using Serilog;
using Serilog.Events;

namespace AppiumLogsHandler.UI.Data
{
    public class AppiumLogsService
    {
        private IOptions<MyOptions> _options;
        public AppiumLogsService(IOptions<MyOptions> options)
        {
            _options = options;
        }
        public async Task<IEnumerable<AppiumLogMessage>> GetAllAppiumMessages(string macminiIp = "127.0.0.1")
        {
            using (HttpClient client = new HttpClient())
            {

                var result = await client.GetAsync($"{_options.Value.ApiUrl}{macminiIp}");
                Console.WriteLine(result.StatusCode);
                var temp = await result.Content.ReadAsAsync<IEnumerable<AppiumLogMessage>>(new[] { new JsonMediaTypeFormatter() });
                return temp;
            }
        }

        public async Task<IEnumerable<AppiumServerDetails>> GetAllMacminis()
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    var logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                    .Enrich.FromLogContext()
                    .WriteTo.Console()
                    .CreateLogger();

                    logger.Information(_options.Value.ApiUrl);

                    var result = await client.GetAsync($"{_options.Value.ApiUrl}allmacminis");
                    Console.WriteLine(result.StatusCode);
                    var macminis = await result.Content.ReadAsAsync<IEnumerable<AppiumServerDetails>>(new[] { new JsonMediaTypeFormatter() });
                    return macminis;
                }
                catch { return null; }
                
            }
        }
    }
}
