using System;
using System.Net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;

namespace AppiumLogsHandler.UI
{
    public class Program
    {
        public static Uri ApiEndpoint;

        public static void Main(string[] args)
        {
            


            //Log.Logger = new LoggerConfiguration()
            //.MinimumLevel.Debug()
            //.MinimumLevel.Override("Microsoft", LogEventLevel.Information)
            //.Enrich.FromLogContext()
            //.WriteTo.Console()
            //.CreateLogger();

            //foreach (string arg in args)
            //{
            //    Log.Information($"Argument : {arg}");
            //}
            //if (args.Length.Equals(0))
            //    throw new Exception("Provide API url parameter.");

            //bool validUrl = Uri.TryCreate(args[0], UriKind.Absolute, out ApiEndpoint)
            //    && ApiEndpoint.Scheme == Uri.UriSchemeHttp;
            //if (!validUrl)
            //    throw new Exception("Provided Api endpoint is not a valid Url");

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                    .UseStartup<Startup>()
                    .UseKestrel(option => option.Listen(IPAddress.Any, 5050));
                });

        
            
    }
}
