using System.Net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace AppiumLogsHandler.Listener
{
    public class Program
    {
        private static int port;
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                    .UseStartup<Startup>()
                    .UseKestrel(option => option.Listen(IPAddress.Any, 5000));
                    //.UseUrls("http://localhost:5000", "http://0.0.0.0:5000");
                    //webBuilder.UseStartup<Startup>().UseUrls("http://localhost:5000", "http://0.0.0.0:5000", "http://10.203.48.70:5000");




                });
    }
}
