﻿namespace AppiumLogsHandler.Listener.Utility
{
    public class MyOptions
    {
        public MyOptions()
        {
            // Set default value.
            MongoDbAddress = "localhost:27017";
        }

        public string MongoDbAddress { get; set; }
        public string DbName { get; set; }
    }
}
