﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppiumLogsHandler.Listener.Utility;
using AppiumLogsHandler.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using Serilog;
using Serilog.Events;
using static AppiumLogsHandler.Listener.Utility.Extensions;
using System.Web;

namespace AppiumLogsHandler.Listener.Controllers
{
    [Route("/")]
    [ApiController]
    public class LogsController : ControllerBase
    {
        private IOptions<MyOptions> _options;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public LogsController(IOptions<MyOptions> options, IHttpContextAccessor httpContextAccessor)
        {
            _options = options;
            _httpContextAccessor = httpContextAccessor;
        }
        //public static IOptions<MyOptions> MyOption(this IServiceCollection services)
        //{
        //    var settings = services.BuildServiceProvider().GetRequiredService<IOptions<MyOptions>>();
        //    return settings;
        //}

        // GET: http://localhost:5000/127.0.0.2
        [HttpGet("{appiumServerIp}")]
        public IEnumerable<AppiumLogMessage> GetMacminiDetails(string appiumServerIp)
        {
            var database = new MongoClient(_options.Value.MongoDbAddress).GetDatabase("logs");
            var logs = database.GetCollection<AppiumLogMessage>(appiumServerIp)
                .AsQueryable()
                .OrderByDescending(l => l.CreationTime)
                .ToList();
            return logs;
        }


        private string ParseAppiumVersion(string text)
        {
            System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex("Welcome to Appium\\s+v(.*)");
            if (rgx.IsMatch(text))
            {
                var match = rgx.Match(text);
                return match.Groups[1].ToString();
            }
            return null;
        }


        [Route("allmacminis")]
        [HttpGet]
        public IEnumerable<AppiumServerDetails> GetAllMacminis()
        {
            var client = new MongoClient(_options.Value.MongoDbAddress).GetDatabase(_options.Value.DbName);
            var collectionNames = client.ListCollectionNames().ToList();
            foreach (var name in collectionNames)
            {
                var collection = client.GetCollection<AppiumLogMessage>(name);
                var messageWithVersion = collection
                    .AsQueryable()
                    .FirstOrDefault(m => m.Message.Contains("Welcome to Appium"));

                if (messageWithVersion == null)
                {
                    yield return new AppiumServerDetails { IpAddress = name, ServerVersion = "Unknown" };
                }
                else
                {
                    yield return new AppiumServerDetails { IpAddress = name, ServerVersion = ParseAppiumVersion(messageWithVersion.Message), StartTime = messageWithVersion.CreationTime };
                }

            }
            //var logger = new LoggerConfiguration()
            //        .MinimumLevel.Debug()
            //        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
            //        .Enrich.FromLogContext()
            //        .WriteTo.Console()
            //        .CreateLogger();

            //logger.Information(_options.Value.MongoDbAddress);

            //return new List<AppiumServerDetails>() { new AppiumServerDetails() { IpAddress = "982938.2398.23.3", ServerVersion = "1.15.1", StartTime = DateTime.Now }, new AppiumServerDetails() { IpAddress = "982938.2398.23.4", ServerVersion = "1.15.1", StartTime = DateTime.Now } };
        }

        //POST: api/Logs
        [HttpPost]
        public async Task Post(AppiumLogMessage message)
        {
            var tt = Request.Headers["X-Client-IP"];
            string IpAddress = null;
            if (Request.Headers["X-Forwarded-For"].ToString().IndexOf(",") > 0)
            {
                IpAddress = Request.Headers["X-Forwarded-For"].ToString().Split(',')[0];
            }
            else
            {
                IpAddress = Request.Headers["X-Forwarded-For"].ToString();
            }


            var externalIp = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
            .Enrich.FromLogContext()
            .WriteTo.Console()
            .CreateLogger();


            Log.Information($"external ip : {externalIp}");
            Log.Information($" ip : {tt}");
            Log.Information($" ip2 : {IpAddress}");



            var msg = new { message, Ip = HttpContext.Connection.RemoteIpAddress.ToString(), CreationTime = DateTime.UtcNow };
            var client = new MongoClient(_options.Value.MongoDbAddress).GetDatabase(_options.Value.DbName);
            var collectionNames = client.ListCollectionNames().ToList<string>();

            if (collectionNames.All(m => !m.Equals(msg.Ip)))
            {
                await client.CreateCollectionAsync(msg.Ip);
                var notificationLogBuilder = Builders<AppiumLogMessage>.IndexKeys;
                var indexModules = new List<CreateIndexModel<AppiumLogMessage>>() {
                    new CreateIndexModel<AppiumLogMessage>(notificationLogBuilder.Descending(x => x.Message)),
                    new CreateIndexModel<AppiumLogMessage>(notificationLogBuilder.Descending(x => x.Level)),
                    new CreateIndexModel<AppiumLogMessage>(notificationLogBuilder.Descending(x => x.CreationTime))
                };
                await client.GetCollection<AppiumLogMessage>(msg.Ip).Indexes.CreateManyAsync(indexModules).ConfigureAwait(false);
            }
            var collection = client.GetCollection<AppiumLogMessage>(msg.Ip);
            collection.InsertOneAsync(new AppiumLogMessage { Message = msg.message.Message, Level = msg.message.Level, CreationTime = msg.message.CreationTime });
        }

        [Route("dummydata")]
        [HttpGet]
        public IEnumerable<AppiumLogMessage> GetDummyData()
        {
            return new List<AppiumLogMessage>()
            {
                new AppiumLogMessage() { Level = "info",Message = "[Appium] Welcome to Appium v1.16.0" },
                new AppiumLogMessage() { Level = "debug",Message = "This is DEBUG" },
                new AppiumLogMessage() { Level = "warn",Message = "This is WARNING" },
                new AppiumLogMessage() { Level = "error",Message = "This is ERROR message" }
            };
        }

        [HttpGet]
        public string DefaultGet()
        {
            return "Logs handler API running ...";
        }


        [HttpGet("deletelogs/{appiumServerIp}")]
        public async Task DeleteAllLogs(string appiumServerIp)
        {
            var database = new MongoClient($"mongodb://{_options.Value.MongoDbAddress}/").GetDatabase("logs");
            var collection = database.GetCollection<BsonDocument>("appium");
            await collection.DeleteManyAsync(Builders<BsonDocument>.Filter.Eq("IpAddress", appiumServerIp));
        }

       
    }
}
