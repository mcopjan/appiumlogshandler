﻿using System.Linq;
using System.Threading.Tasks;
using AppiumLogsHandler.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using NUnit.Framework;


namespace AppiumLogsHandler.Tests
{
    public class Tests
    {
        public static MongoClient MongoClient { get; set; }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            MongoClient = new MongoClient("mongodb://localhost:27017/");
        }

        [Test]
        public async Task InsertIntoMongoDb()
        {
            var database = MongoClient.GetDatabase("logs");
            var collection = database.GetCollection<BsonDocument>("appium");
            await collection.InsertOneAsync(new BsonDocument((new AppiumLogMessage() { Level = "info", Message = "test"}).ToBsonDocument()));
        }

        [Test]
        public void FetchAllExistingMacminis()
        {
            var database = MongoClient.GetDatabase("logs");
            var collection = database.GetCollection<AppiumLogMessage>("appium")
                .AsQueryable();
                //.Select(l => l.IpAddress).Distinct().ToList();

        }

        [Test]
        public void FilterMessagesPerMacmini()
        {
            var database = MongoClient.GetDatabase("logs");
            var collection = database.GetCollection<AppiumLogMessage>("appium")
                .AsQueryable()
                //.Where(l => l.IpAddress.Contains("127.0.0.2"))
                .ToList();

        }

        [Test]
        public void PrepareDbSchema()
        {
            var client = new MongoClient("mongodb://0.0.0.0:32777/");
            client.GetDatabase("logs");

        }

        [Test]
        public void ParseAPPIUMVERSIOM()
        {
            //[Appium] Welcome to Appium v1.16.0
            System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex("Welcome to Appium\\s+v(.*)");
            var match = rgx.Match("[Appium] Welcome to Appium v1.16.0");
            var version = match.Groups[1];


        }

        
    }
}
